//
//  ViewController.m
//  p02-novitske
//
//  Created by Steven Novitske on 2/7/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *t0;
@property (weak, nonatomic) IBOutlet UILabel *t1;
@property (weak, nonatomic) IBOutlet UILabel *t2;
@property (weak, nonatomic) IBOutlet UILabel *t3;
@property (weak, nonatomic) IBOutlet UILabel *t4;
@property (weak, nonatomic) IBOutlet UILabel *t5;
@property (weak, nonatomic) IBOutlet UILabel *t6;
@property (weak, nonatomic) IBOutlet UILabel *t7;
@property (weak, nonatomic) IBOutlet UILabel *t8;
@property (weak, nonatomic) IBOutlet UILabel *t9;
@property (weak, nonatomic) IBOutlet UILabel *t10;
@property (weak, nonatomic) IBOutlet UILabel *t11;
@property (weak, nonatomic) IBOutlet UILabel *t12;
@property (weak, nonatomic) IBOutlet UILabel *t13;
@property (weak, nonatomic) IBOutlet UILabel *t14;
@property (weak, nonatomic) IBOutlet UILabel *t15;
@property (weak, nonatomic) IBOutlet UILabel *banner;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//Up button
- (IBAction)moveUp:(id)sender {
    //Save the tiles' states before movements
    bool movements = false;
    int before[16];
    for(int i=0; i<16; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i+1];
        before[i] = tile.text.intValue;
    }
    //Set up columns
    bool changes = false;
    for(int i=0; i<4; i++) {
        UILabel *top = (UILabel *)[self.view viewWithTag:i];
        UILabel *midTop = (UILabel *)[self.view viewWithTag:i+4];
        UILabel *midBot = (UILabel *)[self.view viewWithTag:i+8];
        UILabel *bottom = (UILabel *)[self.view viewWithTag:i+12];
        if(i==0) top = (UILabel *)[self.view viewWithTag:16];
        //Get rid of any empty spaces between tiles
        if(top.text.length == 0) {
            top.text = midTop.text;
            midTop.text = midBot.text;
            midBot.text = bottom.text;
            bottom.text = @"";
            if(top.text.length == 0) {
                top.text = midTop.text;
                midTop.text = midBot.text;
                midBot.text = @"";
                if(top.text.length == 0) {
                    top.text = midTop.text;
                    midTop.text = @"";
                }
            }
        }
        if(midTop.text.length == 0) {
            midTop.text = midBot.text;
            midBot.text = bottom.text;
            bottom.text = @"";
            if(midTop.text.length == 0) {
                midTop.text = midBot.text;
                midBot.text = @"";
            }
        }
        if(midBot.text.length == 0) {
            midBot.text = bottom.text;
            bottom.text = @"";
        }
        //Merge any matching-touching tiles
        if(top.text.length > 0 && top.text == midTop.text) {
            int newVal = top.text.intValue * 2;
            top.text = [NSString stringWithFormat:@"%i",newVal];
            midTop.text = midBot.text;
            midBot.text = bottom.text;
            bottom.text = @"";
            changes = true;
        }
        if(midTop.text.length > 0 && midTop.text == midBot.text) {
            int newVal = midTop.text.intValue * 2;
            midTop.text = [NSString stringWithFormat:@"%i", newVal];
            midBot.text = bottom.text;
            bottom.text = @"";
            changes = true;
        }
        if(midBot.text.length > 0 && midBot.text == bottom.text) {
            int newVal = midBot.text.intValue * 2;
            midBot.text = [NSString stringWithFormat:@"%i", newVal];
            bottom.text = @"";
            changes = true;
        }
    }
    //Check if any tiles have moved
    for(int i=0; i<16; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i+1];
        if(tile.text.intValue != before[i]) {
            movements = true;
        }
    }
    [self isGameOver];
    if(changes == false && movements) [self addNewTile];
}


//Right button
- (IBAction)moveRight:(id)sender {
    //Save the tiles' states before movements
    bool movements = false;
    int before[16];
    for(int i=0; i<16; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i+1];
        before[i] = tile.text.intValue;
    }
    //Set up rows
    bool changes = false;
    for(int i=1; i<5; i++) {
        UILabel *farRight = (UILabel *)[self.view viewWithTag:i*4-1];
        UILabel *midRight = (UILabel *)[self.view viewWithTag:i*4-2];
        UILabel *midLeft = (UILabel *)[self.view viewWithTag:i*4-3];
        UILabel *farLeft = (UILabel *)[self.view viewWithTag:i*4-4];
        if(i==1) farLeft = (UILabel *)[self.view viewWithTag:16];
        //Get rid of any empty spaces between tiles
        if(farRight.text.length == 0) {
            farRight.text = midRight.text;
            midRight.text = midLeft.text;
            midLeft.text = farLeft.text;
            farLeft.text = @"";
            if(farRight.text.length == 0) {
                farRight.text = midRight.text;
                midRight.text = midLeft.text;
                midLeft.text = @"";
                if(farRight.text.length == 0) {
                    farRight.text = midRight.text;
                    midRight.text = @"";
                }
            }
        }
        if(midRight.text.length == 0) {
            midRight.text = midLeft.text;
            midLeft.text = farLeft.text;
            farLeft.text = @"";
            if(midRight.text.length == 0) {
                midRight.text = midLeft.text;
                midLeft.text = @"";
            }
        }
        if(midLeft.text.length == 0) {
            midLeft.text = farLeft.text;
            farLeft.text = @"";
        }
        //Merge any matching-touching tiles
        if(farRight.text.length > 0 && farRight.text == midRight.text) {
            int newVal = farRight.text.intValue * 2;
            farRight.text = [NSString stringWithFormat:@"%i",newVal];
            midRight.text = midLeft.text;
            midLeft.text = farLeft.text;
            farLeft.text = @"";
            changes = true;
        }
        if(midRight.text.length > 0 && midRight.text == midLeft.text) {
            int newVal = midRight.text.intValue * 2;
            midRight.text = [NSString stringWithFormat:@"%i",newVal];
            midLeft.text = farLeft.text;
            farLeft.text = @"";
            changes = true;
        }
        if(midLeft.text.length > 0 && midLeft.text == farLeft.text) {
            int newVal = midLeft.text.intValue * 2;
            midLeft.text = [NSString stringWithFormat:@"%i",newVal];
            farLeft.text = @"";
            changes = true;
        }
    }
    //Check if any tiles have moved
    for(int i=0; i<16; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i+1];
        if(tile.text.intValue != before[i]) {
            movements = true;
        }
    }
    [self isGameOver];
    if(changes == false && movements) [self addNewTile];
}



//Down button
- (IBAction)moveDown:(id)sender {
    //Save the tiles' states before movements
    bool movements = false;
    int before[16];
    for(int i=0; i<16; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i+1];
        before[i] = tile.text.intValue;
    }
    //Set up columns
    bool changes = false;
    for(int i=0; i<4; i++) {
        UILabel *top = (UILabel *)[self.view viewWithTag:i];
        UILabel *midTop = (UILabel *)[self.view viewWithTag:i+4];
        UILabel *midBot = (UILabel *)[self.view viewWithTag:i+8];
        UILabel *bottom = (UILabel *)[self.view viewWithTag:i+12];
        if(i==0) top = (UILabel *)[self.view viewWithTag:16];
        //Get rid of any empty spaces between tiles
        if(bottom.text.length == 0) {
            bottom.text = midBot.text;
            midBot.text = midTop.text;
            midTop.text = top.text;
            top.text = @"";
            if(bottom.text.length == 0) {
                bottom.text = midBot.text;
                midBot.text = midTop.text;
                midTop.text = @"";
                if(bottom.text.length == 0) {
                    bottom.text = midBot.text;
                    midBot.text = @"";
                }
            }
        }
        if(midBot.text.length == 0) {
            midBot.text = midTop.text;
            midTop.text = top.text;
            top.text = @"";
            if(midBot.text.length == 0) {
                midBot.text = midTop.text;
                midTop.text = @"";
            }
        }
        if(midTop.text.length == 0) {
            midTop.text = top.text;
            top.text = @"";
        }
        //Merge any matching-touching tiles
        if(bottom.text.length > 0 && bottom.text == midBot.text) {
            int newVal = bottom.text.intValue * 2;
            bottom.text = [NSString stringWithFormat:@"%i",newVal];
            midBot.text = midTop.text;
            midTop.text = top.text;
            top.text = @"";
            changes = true;
        }
        if(midBot.text.length > 0 && midBot.text == midTop.text) {
            int newVal = midBot.text.intValue * 2;
            midBot.text = [NSString stringWithFormat:@"%i", newVal];
            midTop.text = top.text;
            top.text = @"";
            changes = true;
        }
        if(midTop.text.length > 0 && midTop.text == top.text) {
            int newVal = midTop.text.intValue * 2;
            midTop.text = [NSString stringWithFormat:@"%i", newVal];
            top.text = @"";
            changes = true;
        }
    }
    //Check if any tiles have moved
    for(int i=0; i<16; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i+1];
        if(tile.text.intValue != before[i]) {
            movements = true;
        }
    }
    [self isGameOver];
    if(changes == false && movements) [self addNewTile];
}



//Left button
- (IBAction)moveLeft:(id)sender {
    //Save the tiles' states before movements
    bool movements = false;
    int before[16];
    for(int i=0; i<16; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i+1];
        before[i] = tile.text.intValue;
    }
    //Set up rows
    bool changes = false;
    for(int i=1; i<5; i++) {
        UILabel *farRight = (UILabel *)[self.view viewWithTag:i*4-1];
        UILabel *midRight = (UILabel *)[self.view viewWithTag:i*4-2];
        UILabel *midLeft = (UILabel *)[self.view viewWithTag:i*4-3];
        UILabel *farLeft = (UILabel *)[self.view viewWithTag:i*4-4];
        if(i==1) farLeft = (UILabel *)[self.view viewWithTag:16];
        //Get rid of any empty spaces between tiles
        if(farLeft.text.length == 0) {
            farLeft.text = midLeft.text;
            midLeft.text = midRight.text;
            midRight.text = farRight.text;
            farRight.text = @"";
            if(farLeft.text.length == 0) {
                farLeft.text = midLeft.text;
                midLeft.text = midRight.text;
                midRight.text = @"";
                if(farLeft.text.length == 0) {
                    farLeft.text = midLeft.text;
                    midLeft.text = @"";
                }
            }
        }
        if(midLeft.text.length == 0) {
            midLeft.text = midRight.text;
            midRight.text = farRight.text;
            farRight.text = @"";
            if(midLeft.text.length == 0) {
                midLeft.text = midRight.text;
                midRight.text = @"";
            }
        }
        if(midRight.text.length == 0) {
            midRight.text = farRight.text;
            farRight.text = @"";
        }
        //Merge any matching-touching tiles
        if(farLeft.text.length > 0 && farLeft.text == midLeft.text) {
            int newVal = farLeft.text.intValue * 2;
            farLeft.text = [NSString stringWithFormat:@"%i",newVal];
            midLeft.text = midRight.text;
            midRight.text = farRight.text;
            farRight.text = @"";
            changes = true;
        }
        if(midLeft.text.length > 0 && midLeft.text == midRight.text) {
            int newVal = midLeft.text.intValue * 2;
            midLeft.text = [NSString stringWithFormat:@"%i",newVal];
            midRight.text = farRight.text;
            farRight.text = @"";
            changes = true;
        }
        if(midRight.text.length > 0 && midRight.text == farRight.text) {
            int newVal = midRight.text.intValue * 2;
            midRight.text = [NSString stringWithFormat:@"%i",newVal];
            farRight.text = @"";
            changes = true;
        }
    }
    //Check if any tiles have moved
    for(int i=0; i<16; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i+1];
        if(tile.text.intValue != before[i]) {
            movements = true;
        }
    }
    [self isGameOver];
    if(changes == false && movements) [self addNewTile];
}



//New Game button
- (IBAction)newGame:(id)sender {
    _banner.text = @"2048";
    //Clear all the tiles
    for(int i=1; i<17; i++) {
        UILabel *t = (UILabel *)[self.view viewWithTag:i];
        t.text = @"";
    }
    //Set two random tiles to 2
    int tile1 = rand() % 16 + 1;
    int tile2 = rand() % 16 + 1;
    while(tile1 == tile2) tile2 = rand() % 16 + 1;
    UILabel *first = (UILabel *)[self.view viewWithTag:tile1];
    UILabel *second = (UILabel *)[self.view viewWithTag:tile2];
    first.text = @"2";
    second.text = @"2";
}



//Adds a '2' tile in place of a random empty tile.
-(void)addNewTile {
    for(int i=0; i<1000; i++) {
        int random = rand() % 16;
        random++;
        UILabel *newTile = (UILabel *)[self.view viewWithTag:random];
        if(newTile.text.length == 0) {
            newTile.text = @"2";
            i = 1000;
        }
    }
}


//Check if there are any moves left
-(void)isGameOver {
    //Check for a 2048 tile
    for(int i=1; i<17; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i];
        if([tile.text  isEqual: @"2048"]) {
            _banner.text = @"You Win!";
            return;
        }
    }
    //Check if tile below matches
    bool gameOver = true;
    for(int i=0; i<12; i++) {
        UILabel *top = (UILabel *)[self.view viewWithTag:i];
        UILabel *bottom = (UILabel *)[self.view viewWithTag:i+4];
        if(i==0) top = (UILabel *)[self.view viewWithTag:16];
        if(top.text.intValue == bottom.text.intValue) {
            gameOver = false;
        }
    }
    //Check if tile to the right matches
    for(int i=0; i<15; i++) {
        UILabel *left = (UILabel *)[self.view viewWithTag:i];
        UILabel *right = (UILabel *)[self.view viewWithTag:i+1];
        if(i==0) left = (UILabel *)[self.view viewWithTag:16];
        if(i % 4 != 3) {
            if(left.text.intValue == right.text.intValue) {
                gameOver = false;
            }
        }
    }
    //Double check to see if there is still an empty tile
    for(int i=1; i<17; i++) {
        UILabel *tile = (UILabel *)[self.view viewWithTag:i];
        if(tile.text.length == 0) gameOver = false;
    }
    if(gameOver) {
        _banner.text = @"Game Over";
    }
}


@end
